package com.sunchildapps.pruebaingresomerqueo.view

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.sunchildapps.pruebaingresomerqueo.R
import com.sunchildapps.pruebaingresomerqueo.model.Movie
import com.sunchildapps.pruebaingresomerqueo.viewmodel.CartViewModel
import kotlinx.android.synthetic.main.cart_fragment.*
import org.koin.android.ext.android.get
import kotlin.concurrent.thread

class CartFragment : Fragment() {

    private var adapter = MoviesMainAdapter()
    private lateinit var viewModel : CartViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.cart_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.setTitle(R.string.cart_title)
        setHasOptionsMenu(true)

        viewModel = get()
        viewModel.getMovies().observe(this, Observer { onMoviesLoaded(it) })

        recyclerCart.layoutManager = LinearLayoutManager(context)
        recyclerCart.adapter = adapter
    }

    private fun onMoviesLoaded(movies: List<Movie>) {
        adapter.setMovies(movies)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.cart_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_delete_cart -> this.deleteCart()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun deleteCart() {
        thread {
            viewModel.deleteCart()
        }
    }
}
