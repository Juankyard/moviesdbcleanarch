package com.sunchildapps.pruebaingresomerqueo.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.sunchildapps.pruebaingresomerqueo.R
import com.sunchildapps.pruebaingresomerqueo.model.Movie
import com.sunchildapps.pruebaingresomerqueo.util.loadImage
import kotlinx.android.synthetic.main.item_movie.view.*


class MoviesMainAdapter(private val movies: MutableList<Movie> = mutableListOf(),
                        private val listener: ((movie: Movie) -> Unit)? = null) :
    RecyclerView.Adapter<MoviesViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviesViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_movie, parent, false)
        return MoviesViewHolder(view)
    }

    override fun getItemCount(): Int {
        return movies.size
    }

    override fun onBindViewHolder(holder: MoviesViewHolder, position: Int) {
        holder.bind(movies[position])

        holder.itemView.setOnClickListener {
            listener?.invoke(movies[position])
        }

    }

    fun setMovies(movieList: List<Movie>) {
        this.movies.clear()
        this.movies.addAll(movieList)
        notifyDataSetChanged()
    }
}

class MoviesViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    fun bind(movie: Movie) = with(itemView) {
        tvTitle.text = movie.title
        tvReleaseDate.text = movie.releaseDate
        movie.posterPath?.let { imageViewMovie.loadImage(it) }
    }
}