package com.sunchildapps.pruebaingresomerqueo.view

import android.os.Bundle
import android.view.*
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.sunchildapps.pruebaingresomerqueo.R
import com.sunchildapps.pruebaingresomerqueo.model.Movie
import com.sunchildapps.pruebaingresomerqueo.util.Const.MOVIE_ARG
import com.sunchildapps.pruebaingresomerqueo.util.showToast
import com.sunchildapps.pruebaingresomerqueo.viewmodel.MainViewModel
import com.sunchildapps.pruebaingresomerqueo.viewmodel.ViewState
import kotlinx.android.synthetic.main.main_fragment.*
import org.koin.android.ext.android.get

class MainFragment : Fragment() {

    private lateinit var viewModel: MainViewModel
    private lateinit var adapter: MoviesMainAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(true)
        activity?.setTitle(R.string.movies_title)

        viewModel = get()
        viewModel.getMovies().observe(this, Observer { onMoviesState(it) })
        viewModel.mainState.observe(this, Observer { onViewState(it) })

        initAdapter()
    }

    private fun initAdapter() {
        adapter = MoviesMainAdapter(listener = onMovieClicked)

        recyclerMovies.layoutManager = LinearLayoutManager(context)
        recyclerMovies.adapter = adapter
    }

    private fun onMoviesState(movies: List<Movie>) {
        adapter.setMovies(movies)
    }

    private fun onViewState(state: ViewState) {
        when (state) {
            is ViewState.Loading -> showProgress(state.isLoading)
            is ViewState.ShowMessage -> showMessage(state.message)
        }
    }

    private fun showMessage(message: String) {
        context?.showToast(message)
    }

    private fun showProgress(isLoading: Boolean) {
        if (isLoading) {
            progressBar.visibility = View.VISIBLE
            recyclerMovies.visibility = View.GONE
        } else {
            progressBar.visibility = View.GONE
            recyclerMovies.visibility = View.VISIBLE
        }
    }

    private val onMovieClicked: (movie: Movie) -> Unit = {
        val movieBundle = bundleOf(MOVIE_ARG to it)
        view?.findNavController()?.navigate(R.id.action_mainFragment_to_detailFragment, movieBundle)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.main_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_goto_cart -> goToCart()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun goToCart() {
        view?.findNavController()?.navigate(R.id.action_mainFragment_to_cartFragment)
    }

}
