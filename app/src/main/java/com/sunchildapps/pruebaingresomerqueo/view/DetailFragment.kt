package com.sunchildapps.pruebaingresomerqueo.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.sunchildapps.pruebaingresomerqueo.R
import com.sunchildapps.pruebaingresomerqueo.model.Movie
import com.sunchildapps.pruebaingresomerqueo.util.Const.MOVIE_ARG
import com.sunchildapps.pruebaingresomerqueo.util.loadImage
import com.sunchildapps.pruebaingresomerqueo.viewmodel.DetailViewModel
import kotlinx.android.synthetic.main.detail_fragment.*
import org.koin.android.ext.android.get
import kotlin.concurrent.thread

class DetailFragment : Fragment() {

    private lateinit var viewModel: DetailViewModel
    private lateinit var movie: Movie

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.detail_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.setTitle(R.string.detail_title)
        viewModel = get()

        movie = arguments?.getParcelable<Movie>(MOVIE_ARG)!!

        initViews()
    }

    private fun initViews() {
        btDeleteFromCart.setOnClickListener {
            thread {
                movie.id?.let { it -> viewModel.deleteMovieFromCart(it) }
            }
        }

        btAddToCart.setOnClickListener {
            thread {
                viewModel.addMovieToCart(movie)
            }
        }

        movie.posterPath?.let { imageViewMovie.loadImage(it) }
        tvTitle.text = movie.title
        tvDescription.text = movie.overview
        tvReleaseDate.text = movie.releaseDate
    }
}
