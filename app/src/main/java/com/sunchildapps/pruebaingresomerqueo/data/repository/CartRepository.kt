package com.sunchildapps.pruebaingresomerqueo.data.repository

import androidx.lifecycle.LiveData
import com.sunchildapps.pruebaingresomerqueo.model.CartMovie

interface CartRepository {

    fun addMovie(movie: CartMovie)

    fun deleteMovie(movieId: Int)

    fun emptyCart()

    fun getMoviesStoredInCart() : LiveData<List<CartMovie>>
}