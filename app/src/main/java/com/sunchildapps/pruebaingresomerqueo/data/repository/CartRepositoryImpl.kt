package com.sunchildapps.pruebaingresomerqueo.data.repository

import androidx.lifecycle.LiveData
import com.sunchildapps.pruebaingresomerqueo.dataBase
import com.sunchildapps.pruebaingresomerqueo.model.CartMovie

class CartRepositoryImpl : CartRepository {

    private val cartDao = dataBase.cartDao()

    override fun addMovie(movie: CartMovie) = cartDao.insert(movie)

    override fun deleteMovie(movieId: Int) = cartDao.delete(movieId)

    override fun emptyCart() = cartDao.emptyCart()

    override fun getMoviesStoredInCart(): LiveData<List<CartMovie>> = cartDao.getAll()
}