package com.sunchildapps.pruebaingresomerqueo.data.repository

import androidx.lifecycle.LiveData
import com.sunchildapps.pruebaingresomerqueo.model.Movie
import io.reactivex.Completable

interface MoviesLocalRepository {

    fun getSavedMovies(): LiveData<List<Movie>>

    fun saveMovies(movies: List<Movie>) : Completable
}