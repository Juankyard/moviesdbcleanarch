package com.sunchildapps.pruebaingresomerqueo.data.repository

import com.sunchildapps.network.RetrofitClient
import com.sunchildapps.pruebaingresomerqueo.model.Movie
import com.sunchildapps.pruebaingresomerqueo.util.mapResponseToMovie
import io.reactivex.Single

class MoviesRemoteRepositoryImpl (private val retrofitClient: RetrofitClient) :
    MoviesRemoteRepository {
    override fun getMovies(): Single<List<Movie>> {
        return retrofitClient.getMovies().map { moviesResponse ->
            moviesResponse.results?.map { it.mapResponseToMovie() }
        }
    }

    override fun getLinkedMovie(movieId: String): Single<Movie> {
        return retrofitClient.getLinkedMovie(movieId).map {
            val movie = Movie(it.id, it.title, it.posterPath, it.overview, it.releaseDate)
            movie
        }
    }
}