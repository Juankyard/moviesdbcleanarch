package com.sunchildapps.pruebaingresomerqueo.data.repository

import com.sunchildapps.pruebaingresomerqueo.model.Movie
import io.reactivex.Single

interface MoviesRemoteRepository {

    fun getMovies(): Single<List<Movie>>

    fun getLinkedMovie(movieId: String): Single<Movie>
}