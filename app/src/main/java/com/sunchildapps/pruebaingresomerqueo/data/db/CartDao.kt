package com.sunchildapps.pruebaingresomerqueo.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.sunchildapps.pruebaingresomerqueo.model.CartMovie

@Dao
interface CartDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(movie: CartMovie)

    @Query("SELECT * FROM cartmovie")
    fun getAll(): LiveData<List<CartMovie>>

    @Query("DELETE FROM cartmovie WHERE id = :id")
    fun delete(id: Int?)

    @Query("DELETE FROM cartmovie")
    fun emptyCart()
}