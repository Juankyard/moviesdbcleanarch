package com.sunchildapps.pruebaingresomerqueo.data.repository

import androidx.lifecycle.LiveData
import com.sunchildapps.pruebaingresomerqueo.dataBase
import com.sunchildapps.pruebaingresomerqueo.model.Movie
import io.reactivex.Completable
import kotlin.concurrent.thread

class MoviesLocalRepositoryImpl : MoviesLocalRepository {

    private val moviesDao = dataBase.movieDao()

    override fun getSavedMovies(): LiveData<List<Movie>> = moviesDao.getAll()

    override fun saveMovies(movies: List<Movie>): Completable {
        return Completable.fromAction {
            thread {
                for (movie in movies)
                    moviesDao.insert(movie)
            }
        }
    }
}