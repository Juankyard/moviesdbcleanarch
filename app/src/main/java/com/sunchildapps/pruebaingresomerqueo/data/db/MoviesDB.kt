package com.sunchildapps.pruebaingresomerqueo.data.db

import android.app.Application
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.sunchildapps.pruebaingresomerqueo.model.CartMovie
import com.sunchildapps.pruebaingresomerqueo.model.Movie

@Database(entities = [Movie::class, CartMovie::class], version = 1)
@TypeConverters(GenreIdConverter::class)
abstract class MoviesDB : RoomDatabase() {

    abstract fun movieDao(): MoviesDao

    abstract fun cartDao(): CartDao

    companion object {
        private const val DB_NAME = "MoviesDatabase"
        private var INSTANCE: MoviesDB? = null

        fun getInstance(application: Application): MoviesDB {
            synchronized(Any()) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(application, MoviesDB::class.java, DB_NAME)
                        .build()
                }
            }
            return INSTANCE!!
        }
    }
}