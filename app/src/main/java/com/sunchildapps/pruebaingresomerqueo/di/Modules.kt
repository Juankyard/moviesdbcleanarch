package com.sunchildapps.pruebaingresomerqueo.di

import com.sunchildapps.network.RetrofitClient
import com.sunchildapps.pruebaingresomerqueo.data.repository.*
import com.sunchildapps.pruebaingresomerqueo.interactor.*
import com.sunchildapps.pruebaingresomerqueo.viewmodel.CartViewModel
import com.sunchildapps.pruebaingresomerqueo.viewmodel.DetailViewModel
import com.sunchildapps.pruebaingresomerqueo.viewmodel.MainViewModel
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val applicationModule = module(override = true) {
    single { RetrofitClient() }
    factory<MoviesRemoteRepository> { MoviesRemoteRepositoryImpl(get()) }
    factory<MoviesLocalRepository> { MoviesLocalRepositoryImpl() }
    factory { GetAllMoviesUsecase(get(), get()) }
    viewModel { MainViewModel(get()) }

    factory<CartRepository> { CartRepositoryImpl() }
    factory { AddMovieToCartUsecase(get()) }
    factory { DeleteMovieFromCartUsecase(get()) }
    viewModel { DetailViewModel(get(), get()) }

    factory { GetMoviesStoredInCartUsecase(get()) }
    factory { DeleteAllMoviesFromCartUsecase(get()) }
    viewModel { CartViewModel(get(), get()) }
}