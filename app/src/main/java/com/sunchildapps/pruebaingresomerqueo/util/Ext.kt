package com.sunchildapps.pruebaingresomerqueo.util

import android.content.Context
import android.widget.ImageView
import android.widget.Toast
import com.squareup.picasso.Picasso
import com.sunchildapps.network.RetrofitClient

fun ImageView.loadImage(url: String) =
    Picasso.get().load(RetrofitClient.IMAGE_URL + url).into(this)

fun Context.showToast(message: String, duration: Int = Toast.LENGTH_SHORT) = Toast.makeText(this, message, duration).show()