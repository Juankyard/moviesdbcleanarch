package com.sunchildapps.pruebaingresomerqueo.util

import com.sunchildapps.network.response.SingleMovieResponse
import com.sunchildapps.pruebaingresomerqueo.model.CartMovie
import com.sunchildapps.pruebaingresomerqueo.model.Movie

fun SingleMovieResponse.mapResponseToMovie() = Movie(
    this.id,
    this.title,
    this.posterPath,
    this.overview,
    this.releaseDate
)

fun Movie.mapToCartMovie() = CartMovie(
    this.id,
    this.title,
    this.posterPath,
    this.overview,
    this.releaseDate
)

fun CartMovie.mapToMovie() = Movie(
    this.id,
    this.title,
    this.posterPath,
    this.overview,
    this.releaseDate
)