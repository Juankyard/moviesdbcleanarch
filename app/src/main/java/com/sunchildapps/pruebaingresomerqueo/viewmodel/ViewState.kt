package com.sunchildapps.pruebaingresomerqueo.viewmodel

sealed class ViewState {
    class Loading(val isLoading: Boolean) : ViewState()
    class ShowMessage(val message: String) : ViewState()
}