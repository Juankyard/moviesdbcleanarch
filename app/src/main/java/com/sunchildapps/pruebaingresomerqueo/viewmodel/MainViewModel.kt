package com.sunchildapps.pruebaingresomerqueo.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.sunchildapps.pruebaingresomerqueo.interactor.GetAllMoviesUsecase
import com.sunchildapps.pruebaingresomerqueo.model.Movie
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MainViewModel (private val getAllMoviesUsecase: GetAllMoviesUsecase) : ViewModel() {

    private val observer = CompositeDisposable()
    private val allMovies = MediatorLiveData<List<Movie>>()
    private var viewState: MutableLiveData<ViewState> = MutableLiveData()
    val mainState: LiveData<ViewState> = viewState

    init {
        getAllMovies()
    }

    fun getMovies() = allMovies

    private fun getAllMovies() {
        allMovies.addSource(getAllMoviesUsecase.getSavedMovies()) { savedMovies ->
            if (savedMovies.isEmpty()) {
                viewState.value = ViewState.Loading(true)
                val subscription = getAllMoviesUsecase.getMoviesFrom()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ movies ->
                        this.allMovies.postValue(movies)
                        onMoviesLoaded()
                    }, { error ->
                        showError(error.message.toString())
                    })
                observer.add(subscription)
            } else {
                this.allMovies.postValue(savedMovies)
                onMoviesLoaded()
            }
        }
    }

    private fun showError(error: String) {
        viewState.value = (ViewState.ShowMessage(error))
    }

    private fun onMoviesLoaded() {
        viewState.value = ViewState.Loading(false)
    }

    override fun onCleared() {
        super.onCleared()
        observer.dispose()
    }
}