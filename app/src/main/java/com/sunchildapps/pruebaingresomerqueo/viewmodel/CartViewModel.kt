package com.sunchildapps.pruebaingresomerqueo.viewmodel

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import com.sunchildapps.pruebaingresomerqueo.interactor.DeleteAllMoviesFromCartUsecase
import com.sunchildapps.pruebaingresomerqueo.interactor.GetMoviesStoredInCartUsecase
import com.sunchildapps.pruebaingresomerqueo.model.Movie
import com.sunchildapps.pruebaingresomerqueo.util.mapToMovie

class CartViewModel(
    private val deleteAllMoviesFromCartUsecase: DeleteAllMoviesFromCartUsecase,
    private val getMoviesStoredInCartUsecase: GetMoviesStoredInCartUsecase
) : ViewModel() {

    private val cartMovies = MediatorLiveData<List<Movie>>()

    init{
        getSavedMoviesFromCart()
    }

    fun getMovies() = cartMovies

    private fun getSavedMoviesFromCart() {
        cartMovies.addSource(getMoviesStoredInCartUsecase.invoke()) { savedMovies ->
            cartMovies.postValue(savedMovies.map { cartMovie ->
                cartMovie.mapToMovie()
            })
        }
    }

    fun deleteCart() {
        deleteAllMoviesFromCartUsecase.invoke()
    }
}