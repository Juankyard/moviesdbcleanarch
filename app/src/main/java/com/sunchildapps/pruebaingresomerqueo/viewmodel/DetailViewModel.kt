package com.sunchildapps.pruebaingresomerqueo.viewmodel

import androidx.lifecycle.ViewModel
import com.sunchildapps.pruebaingresomerqueo.interactor.AddMovieToCartUsecase
import com.sunchildapps.pruebaingresomerqueo.interactor.DeleteMovieFromCartUsecase
import com.sunchildapps.pruebaingresomerqueo.model.Movie

class DetailViewModel(
    private val addMovieToCartUsecase: AddMovieToCartUsecase,
    private val deleteMovieFromCartUsecase: DeleteMovieFromCartUsecase
) : ViewModel() {

    fun addMovieToCart(movie: Movie) {
        addMovieToCartUsecase.invoke(movie)
    }

    fun deleteMovieFromCart(movieId: Int) {
        deleteMovieFromCartUsecase.invoke(movieId)
    }
}