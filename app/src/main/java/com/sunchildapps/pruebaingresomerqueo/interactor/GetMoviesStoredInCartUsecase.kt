package com.sunchildapps.pruebaingresomerqueo.interactor

import com.sunchildapps.pruebaingresomerqueo.data.repository.CartRepository

class GetMoviesStoredInCartUsecase (private val cartRepository: CartRepository) {

    operator fun invoke() = cartRepository.getMoviesStoredInCart()
}