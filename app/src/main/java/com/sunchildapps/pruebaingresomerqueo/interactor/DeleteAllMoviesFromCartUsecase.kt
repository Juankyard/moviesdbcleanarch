package com.sunchildapps.pruebaingresomerqueo.interactor

import com.sunchildapps.pruebaingresomerqueo.data.repository.CartRepository

class DeleteAllMoviesFromCartUsecase (private val cartRepo: CartRepository) {

    operator fun invoke() = cartRepo.emptyCart()
}