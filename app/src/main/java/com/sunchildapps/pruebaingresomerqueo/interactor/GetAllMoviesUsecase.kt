package com.sunchildapps.pruebaingresomerqueo.interactor

import androidx.lifecycle.LiveData
import com.sunchildapps.pruebaingresomerqueo.data.repository.MoviesLocalRepository
import com.sunchildapps.pruebaingresomerqueo.data.repository.MoviesRemoteRepository
import com.sunchildapps.pruebaingresomerqueo.model.Movie
import io.reactivex.Single

class GetAllMoviesUsecase(
    private val moviesRemoteRepository: MoviesRemoteRepository,
    private val localRepository: MoviesLocalRepository
) {

    fun getMoviesFrom(): Single<List<Movie>> = moviesRemoteRepository.getMovies()
            .flatMap { movies ->
                localRepository.saveMovies(movies)
                    .andThen(Single.just(movies))

            }

    fun getSavedMovies(): LiveData<List<Movie>> = localRepository.getSavedMovies()
}