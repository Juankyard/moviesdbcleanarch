package com.sunchildapps.pruebaingresomerqueo.interactor

import com.sunchildapps.pruebaingresomerqueo.data.repository.CartRepository
import com.sunchildapps.pruebaingresomerqueo.model.Movie
import com.sunchildapps.pruebaingresomerqueo.util.mapToCartMovie

class AddMovieToCartUsecase(private val cartRepo: CartRepository) {
    operator fun invoke(movie: Movie) = cartRepo.addMovie(movie.mapToCartMovie())
}