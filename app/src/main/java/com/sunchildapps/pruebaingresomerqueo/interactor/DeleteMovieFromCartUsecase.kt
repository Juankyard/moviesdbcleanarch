package com.sunchildapps.pruebaingresomerqueo.interactor

import com.sunchildapps.pruebaingresomerqueo.data.repository.CartRepository

class DeleteMovieFromCartUsecase (private val cartRepo: CartRepository) {

    operator fun invoke (movie: Int) = cartRepo.deleteMovie(movie)
}
