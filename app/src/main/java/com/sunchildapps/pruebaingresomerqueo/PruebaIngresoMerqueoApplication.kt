package com.sunchildapps.pruebaingresomerqueo

import android.app.Application
import com.sunchildapps.pruebaingresomerqueo.data.db.MoviesDB
import com.sunchildapps.pruebaingresomerqueo.di.applicationModule
import org.koin.android.ext.android.startKoin

lateinit var dataBase: MoviesDB

class PruebaIngresoMerqueoApplication : Application() {

    companion object {
        lateinit var INSTANCE: PruebaIngresoMerqueoApplication
    }

    init {
        INSTANCE = this
    }

    override fun onCreate() {
        super.onCreate()
        dataBase = MoviesDB.getInstance(this)
        startKoin(this, listOf(applicationModule))
        INSTANCE = this
    }
}