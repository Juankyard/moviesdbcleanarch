## PruebaIngresoMerqueo ##

This is a demonstration Android project, created as an example of implementation of different libraries, and clean architecture.

This project simulates a movies shopping cart, by consuming a REST API from the [Movie Database site ](https://www.themoviedb.org/documentation/api) 

It follows the MVVM architecture pattern, and uses  [Android Architeture Components](https://developer.android.com/topic/libraries/architecture) like Room, LiveData, Navigation, ViewModel.


## How to use ##
Download or clone this repository, then use Android Studio to open it. It is provided with an API key just for demonstration purposes, in case of expiration, 
a new API key must be provided from the  [Movie Database site ](https://www.themoviedb.org/documentation/api).
Set the new API key on the `` network `` Android module, on its ``build.gradle`` file:


``buildConfigField "String", "API_KEY", '"NEW_API_KEY"' `` 

* Execute the project on an Android device or emulator, and provide Internet connection to it.
* As it runs, it should show a list of movies provided by the network service.
* To enter a movie detail, just tap the movie of your choice, it will lead you to the movie detail.
* On the movie detail screen, there's a brief overview and also there are two buttons, one for adding the movie to the cart, an other to remove it.
* Press back to return to the movies list.
* In the upper right side, there's a menu button, use it to go to the Cart, this screen will show you the movies you have stored on it.
* On the cart screen, there's another menu button, to empty your cart. Press back to return to the movies list

If you enter the application the first time with an Internet connection, data will be saved on the device, and later if you lose your connection you can still see the movies on the list, and also 
those you have stor in your cart.

## Structure ##
The ``develop`` branch contains the most features following the requirements, except for the Navigation implementation, whereas this branch was made with the multiple Activities Approach .
However, on the ``master`` contains the implementation of Navigation, and follows the single-Activity approach.

* There's a ``network`` Android module that provides a REST API client, by using [Retrofit](https://https://square.github.io/retrofit/)
* Dependency injection is made with [Koin](https://insert-koin.io/)
* Background tasks made with [RxJava](https://github.com/ReactiveX/RxJava) and [RxAndroid](https://github.com/ReactiveX/rxandroid), and also with ``LiveData``
 from the [Android Architeture Components](https://developer.android.com/topic/libraries/architecture)
* Database management is made with ``Room`` from the [Android Architeture Components](https://developer.android.com/topic/libraries/architecture)

## How it looks ##
![picture](https://bitbucket.org/Juankyard/pruebaingresomerqueo/raw/97a67849cf3434bcb305a32a55b3e7c51c9d9e90/proof.gif)

## TODO ##

* Unit testing, also providing test coverage.
* For the API REST client, it is also alternative the use of the [Volley](https://github.com/google/volley) library
* Deep linking
