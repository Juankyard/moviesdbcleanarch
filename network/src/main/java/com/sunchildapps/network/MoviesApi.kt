package com.sunchildapps.network

import com.sunchildapps.network.response.MoviesResponse
import com.sunchildapps.network.response.SingleMovieResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MoviesApi {

    @GET("discover/movie")
    fun getMovies(@Query("api_key") api_key: String): Single<MoviesResponse>

    @GET("movie/{movie_id}")
    fun getLinkedMovie(@Path("movie_id") movie_id: String, @Query("api_key") api_key: String): Single<SingleMovieResponse>
}