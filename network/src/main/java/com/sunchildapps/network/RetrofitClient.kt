package com.sunchildapps.network

import com.sunchildapps.network.response.MoviesResponse
import com.sunchildapps.network.response.SingleMovieResponse
import io.reactivex.Single
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitClient {

    private val moviesApi: MoviesApi
    companion object {
        const val IMAGE_URL = BuildConfig.IMAGE_URL
    }


    init {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val builder = OkHttpClient.Builder()
        val okHttpClient = builder.addInterceptor(interceptor).build()
        val retrofit = Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build()
        moviesApi = retrofit.create(MoviesApi::class.java)
    }

    fun getMovies(): Single<MoviesResponse> {
        return moviesApi.getMovies(BuildConfig.API_KEY)
    }

    fun getLinkedMovie(movieId: String): Single<SingleMovieResponse> {
        return moviesApi.getLinkedMovie(movieId, BuildConfig.API_KEY)
    }
}